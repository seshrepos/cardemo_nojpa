package com.sboot.car.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Car {
	private int id = increment();
	private String make;
	private String model;
	private String colour;
	private int year;
	
	private static int counter = 0;

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public int increment () {
		return counter++;
	}
	

}
