package com.sboot.car;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class CardemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardemoApplication.class, args);
	}


	@Bean
	public Docket swaggerConfiguration() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.paths(PathSelectors.ant("/cars/*"))
				.apis(RequestHandlerSelectors.basePackage("com.sboot"))
				.build()
				.apiInfo(apiInformation());
	}
	
	private ApiInfo apiInformation() {
		return new ApiInfo(
				"Car Demo API",
				"Car Demo API Created for ECS task - No data persistence",
				"1.0.0",
				"Free use",
				new springfox.documentation.service.Contact("Sesh Andukuri", "http://sesh", "asesha@yahoo.com"),
				"API Licence",
				"Sesh Andukuri",
				Collections.emptyList());
				
	}
	
}
