package com.sboot.car.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sboot.car.domain.Car;
//import com.sboot.car.service.CarService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/cars")
public class CarController {

	ConcurrentMap<Integer, Car> cars = new ConcurrentHashMap<>();

	@GetMapping("/{id}") 
	@ApiOperation(value = "Find Car by Id",
	notes = "Provide a Car ID to lookup for specific Car from Car List",
	response = Car.class)
	public ResponseEntity<?> getCar(@PathVariable int id)  {
		if (!cars.containsKey(id)) {
			return new ResponseEntity<>(" This car Doesn't exist", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(cars.get(id), HttpStatus.OK);
	}
	
	@GetMapping("/all")
	@ApiOperation(value = "Show all cars ",
	notes = "Shows All available Cars",
	response = Car.class)
	public ResponseEntity<?>  getAllCars(){
		if(cars.isEmpty()) {
			return new ResponseEntity(" No cars added to List", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>((cars.values()),HttpStatus.OK);
	}
	
	@PostMapping("/add")
	@ApiOperation(value = "Add a car to the List ",
	notes = "Adds new car to the list with given details",
	response = Car.class)
	public Car addCar(@RequestBody Car car){
		
		cars.put( car.getId(), car);
		return car;
		
	}
}
